//! Maps enumeration. Extension: .loadlist, .loadlistlist

use encoding_rs::SHIFT_JIS;

#[derive(Debug, Default)]
pub struct Entry {
    pub path: String,
    pub name: String,
}

impl Entry {
    pub fn correct_map_path(&self) -> String {
        assert!(self.path.starts_with("map:"));
        self.path.replace("map:/", "/map/")
    }

    /// Map file name w/o extension and path
    ///
    /// Ex.: `map:/MapStudio/m14_00_00_00.msb` becomes `m14_00_00_00`
    pub fn map_basename(&self) -> String {
        assert_eq!(&self.path[0..15], "map:/MapStudio/");
        assert_eq!(&self.path[27..31], ".msb");

        self.path[15..27].to_string()
    }
}

pub fn from(buf: Vec<u8>) -> Vec<Entry> {
    let mut rz = Vec::<Entry>::new();

    // true = append path to entry
    // false = append payload and switch to new entry
    let mut appending_path = true;
    let mut entry = Entry::default();

    let mut ptr = 0;

    for (idx, val) in buf.iter().enumerate() {
        if appending_path && *val == 0x09 {
            // Skip CRLF
            while buf[ptr] == b'\r' {
                ptr += 2;
            }

            let l = &buf.as_slice()[ptr..idx];
            let m = std::str::from_utf8(l).unwrap();
            entry.path.push_str(m);

            ptr = idx + 1;
            appending_path = false;
        }

        if !appending_path && *val == b'\r' {
            assert_eq!(buf[ptr], b'#');

            ptr += 1; // Skip "#" at start of description

            let (s, _, _) = SHIFT_JIS.decode(&buf.as_slice()[ptr..idx]);
            entry.name = s.to_string();

            rz.push(entry);
            entry = Entry::default();

            ptr = idx;
            appending_path = true;
        }
    }

    return rz;
}
