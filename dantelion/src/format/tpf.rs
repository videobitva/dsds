//! A multi-file texture container used throughout the series. Extension: .tpf
//!
//! File spec and comments taken from [SoulsFormats](https://github.com/JKAnderson/SoulsFormats/)

use std::{mem::size_of, slice};

use crate::Validate;

const MAGIC_HEADER: [u8; 4] = *b"TPF\0";

#[repr(C)]
pub struct TpfHeader {
    magic: [u8; 4],

    pub data_size: i32,
    pub texture_count: i32,

    /// The platform this TPF will be used on.
    pub platform: Platform,
    pub flags: u8,

    /// Indicates encoding used for texture names.
    pub encoding: Encoding,
    padding: u8,
}

impl Validate for TpfHeader {
    fn validate(&self) {
        assert_eq!(self.magic, MAGIC_HEADER);
        assert_eq!(self.padding, 0);
    }
}

/// The platform of the game a TPF is for.
#[derive(Debug)]
#[repr(u8)]
pub enum Platform {
    /// Headered DDS with minimal metadata.
    PC = 0,

    /// Headerless DDS with pre-DX10 metadata.
    Xbox360 = 1,

    /// Headerless DDS with pre-DX10 metadata.
    PS3 = 2,

    /// Headerless DDS with DX10 metadata.
    PS4 = 4,

    /// Headerless DDS with DX10 metadata.
    Xbone = 5,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Encoding {
    ShiftJis1 = 0,
    Utf16 = 1,
    ShiftJis2 = 2,
}

#[repr(C)]
pub struct TextureHeader {
    file_offset: u32,
    pub file_size: i32,

    pub format: u8,
    pub tex_type: u8,
    pub mipmaps: u8,
    pub flags: u8,
}

pub struct Tpf<'a> {
    data: Vec<u8>,

    pub header: &'a TpfHeader,
    pub texture_headers: &'a [TextureHeader],
}

impl<'a> Tpf<'a> {
    pub fn new(data: Vec<u8>) -> Tpf<'a> {
        let mut pointer = data.as_ptr();

        let header = unsafe { (pointer as *const TpfHeader).as_ref().unwrap() };
        header.validate();
        pointer = unsafe { pointer.add(size_of::<TpfHeader>()) };

        let texture_headers = unsafe {
            slice::from_raw_parts(
                pointer as *const TextureHeader,
                header.texture_count as usize,
            )
        };

        Tpf {
            data,
            header,
            texture_headers,
        }
    }

    pub fn texture_iter<'b>(&'b self) -> TextureIter<'b> {
        TextureIter {
            index: self,
            current_idx: -1,
        }
    }

    /// Access raw dds data
    pub fn dds(&self, index: i32) -> &[u8] {
        let tex = &self.texture_headers[index as usize];
        let start = tex.file_offset as usize;
        let end = start + tex.file_size as usize;
        &self.data[start..end]
    }
}

pub struct Texture<'a> {
    tpf: &'a Tpf<'a>,
    pub header: &'a TextureHeader,
}

impl<'a> Texture<'a> {
    pub fn data(&self) -> &[u8] {
        let start = self.header.file_offset as usize;
        let end = start + self.header.file_size as usize;
        &self.tpf.data[start..end]
    }
}

pub struct TextureIter<'a> {
    index: &'a Tpf<'a>,
    current_idx: i32,
}

impl<'a> Iterator for TextureIter<'a> {
    type Item = Texture<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        self.current_idx += 1;

        let i = self.current_idx as usize;
        if i < self.index.texture_headers.len() {
            return Some(Texture {
                tpf: self.index,
                header: &self.index.texture_headers[i],
            });
        }

        None
    }
}
