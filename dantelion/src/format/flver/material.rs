//! A reference to an MTD file, specifying textures to use.

#[repr(C)]
pub struct Material {
    /// Identifies the mesh that uses this material, may include keywords that determine hideable parts
    ///
    /// Encoded in UTF-16 or JIS depending on unicode flag
    pub name_offset: i32,

    /// Virtual path to an MTD file
    ///
    /// Encoded in UTF-16 or JIS depending on unicode flag
    pub mtd_offset: i32,

    /// Textures used by this material
    pub texture_count: i32,
    pub texture_index: i32,

    /// Unknown
    pub flags: i32,

    /// Index to the flver's list of GX lists.
    pub gx_offset: i32,

    /// Unknown; only used in Sekiro.
    unknown: i32,

    padding: i32,
}

#[repr(u32)]
pub enum Flags {
    /// Seems to be always disabled
    Unused = 1 << 0,

    /// Never mentioned w/o diffuse texture
    DiffuseOnly09 = 1 << 9,

    /// Never mentioned w/o diffuse texture
    DiffuseOnly10 = 1 << 10,
}

impl Material {
    pub fn validate(&self) {
        assert_eq!(self.padding, 0);

        assert_eq!(self.flags & 0b1, 0);
        assert_eq!(self.flags >> 11, 0);
    }
}
