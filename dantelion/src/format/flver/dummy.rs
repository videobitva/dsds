//! "Dummy polygons" used for hit detection, particle effect locations, and much more.

use crate::Vec3;

#[repr(C)]
pub struct Dummy {
    /// Location of the dummy point.
    position: Vec3,

    /// 4-byte color. Use unknown
    ///
    /// Byte order is uncertain
    ///
    /// Presumably, BGRA in ver.0x20010 and ARGB otherwise
    color: [u8; 4],

    /// Vector indicating the dummy point's forward direction.
    forward: Vec3,

    /// Indicates the type of dummy point this is (hitbox, sfx, etc).
    reference_id: i16,

    /// Index of a bone that the dummy point is initially transformed to before binding to the attach bone.
    parent_bone_index: i16,

    /// Vector indicating the dummy point's upward direction.
    upward: Vec3,

    /// Index of the bone that the dummy point follows physically.
    attach_bone_index: i16,

    /// Unknown
    flag1: bool,

    /// If false, the upward vector is not read.
    use_upward_vector: bool,

    /// Unknown; only used in Sekiro.
    unknown_1: i32,

    /// Unknown; only used in Sekiro.
    unknown_2: i32,

    padding: [u32; 2],
}

impl Dummy {
    pub fn validate(&self) {
        assert_eq!(self.padding, [0_u32; 2]);
    }
}
