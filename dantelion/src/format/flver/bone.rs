//! A joint available for vertices to be attached to

use crate::Vec3;

#[repr(C)]
pub struct Bone {
    /// Translation of this bone
    translation: Vec3,

    /// Corresponds to the name of a bone in the parent skeleton, if present
    ///
    /// Encoded in UTF-16 or JIS depending on unicode flag
    pub name_offset: i32,

    /// Rotation of this bone; euler radians in XZY order
    rotation: Vec3,

    /// Index of the parent in this FLVER's bone collection, or -1 for none
    parent_index: i16,

    /// Index of the first child in this FLVER's bone collection, or -1 for none
    child_index: i16,

    /// Scale of this bone
    scale: Vec3,

    /// Index of the next child of this bone's parent, or -1 for none
    next_sibling_index: i16,

    /// Index of the previous child of this bone's parent, or -1 for none
    prev_sibling_index: i16,

    /// Minimum extent of the vertices weighted to this bone
    bbox_min: Vec3,

    /// Unknown; only 0 or 1 before Sekiro.
    unknown: i32,

    ///  Maximum extent of the vertices weighted to this bone
    bbox_max: Vec3,

    giga_padding: [i32; 13],
}

impl Bone {
    pub fn validate(&self) {
        assert_eq!(self.giga_padding, [0; 13]);
    }
}
