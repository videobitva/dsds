//! Group - model

use crate::{magic, Validate};

use super::model_part_type::ModelPartType;

const SIB_NAME_PREFIX: &[u8; 7] = b"N:\\FRPG";

///
#[repr(C)]
pub struct ModelEntry {
    name_offset: i32,
    pub entry_type: ModelPartType,
    pub id: i32,

    /// SIBs are not present in release data
    sib_offset: i32,

    pub instance_count: i32,
    padding: [i32; 3],
}

impl ModelEntry {
    pub fn sib(&self) -> Option<&str> {
        unsafe { magic::ascii_by_relative_offset(self, self.sib_offset) }
    }

    pub fn name(&self) -> &str {
        unsafe { magic::ascii_by_relative_offset(self, self.name_offset) }.unwrap()
    }

    pub fn map_flver_path(&self, map_name: &str) -> String {
        assert_eq!(&map_name[0..1], "m");

        let level_no = &map_name[1..3];
        let model_name = self.name();

        match self.entry_type {
            ModelPartType::MapPiece => format!("/map/{map_name}/{model_name}A{level_no}.flver.dcx"),
            _ => panic!("Map flver only supported for map type"),
        }
    }

    pub fn object_bnd_path(&self) -> String {
        let model_name = self.name();

        match self.entry_type {
            ModelPartType::Object => format!("/obj/{model_name}.objbnd"),
            _ => panic!("Map flver only supported for map type"),
        }
    }
}

impl Validate for ModelEntry {
    fn validate(&self) {
        assert_eq!(self.padding, [0i32; 3]);
    }
}
