//! Group - region aka PointParam

use crate::Validate;

/// Aka PointParam
///
/// Point or trigger volume used by scripts and events.
#[repr(C)]
pub struct RegionEntry {
    name_offset: i32,

    padding_1: i32,

    /// Identifies the region in external files
    pub id: i32,

    shape_type: u32,

    /// Location of the region.
    pub position: [i32; 3],

    /// Rotation of the region, in degrees
    pub rotation: [i32; 3],

    unknown_offset_a: i32,
    unknown_offset_b: i32,
    shape_data_offset: i32,
    entity_data_offset: i32,
    padding_2: i32,
}

impl Validate for RegionEntry {
    fn validate(&self) {
        assert_eq!(self.padding_1, 0);
        assert_eq!(self.padding_2, 0);
    }
}
