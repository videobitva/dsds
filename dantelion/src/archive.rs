//! Handles Binder stuff (bhd/bdt, bhf/bdf), provides access to archive data by hash or path

use std::{fs::File, io::Read};

use crate::{
    config::UserConfig,
    fhash,
    format::{
        bhd5::{Bhd5, Record},
        bhf3::Bhf3,
    },
    magic::BetterSeek,
};

/// Top-level archive, encapsulates .bhd5 + .bdt files access
pub struct RootArchive<'a> {
    index: Bhd5<'a>,
    data: File,
}

impl<'a> RootArchive<'a> {
    pub fn new(cfg: &UserConfig, index: u8) -> RootArchive<'a> {
        let mut header_file = cfg.file(&format!("dvdbnd{}.bhd5", index)).unwrap();
        let mut header_data = Vec::<u8>::new();
        header_file.read_to_end(&mut header_data).unwrap();

        RootArchive {
            index: Bhd5::new(header_data),
            data: cfg.file(&format!("dvdbnd{}.bdt", index)).unwrap(),
        }
    }

    /// Prints comma-separated file offset, padded file size and file name hash
    pub fn print_human_index(&self) {
        for record in self.index.record_iter() {
            println!(
                "{:x?},{},{}",
                record.file_offset,
                record.get_padded_file_size(),
                fhash::rev(record.file_name_hash)
            );
        }
    }

    pub fn record_by_name(&self, path: &str) -> Option<&Record> {
        self.index.get_record_by_hash(fhash::hash(path))
    }

    pub fn record_by_hash(&self, hash: u32) -> Option<&Record> {
        self.index.get_record_by_hash(hash)
    }

    pub fn data_by_name(&self, path: &str) -> Option<Vec<u8>> {
        self.data_by_hash(fhash::hash(path))
    }

    pub fn data_by_hash(&self, hash: u32) -> Option<Vec<u8>> {
        let record = self.record_by_hash(hash);
        if record.is_none() {
            return None;
        }

        let mut data = self.data.try_clone().unwrap();
        let record = record.unwrap();
        data.seek_to(record.file_offset as i64).unwrap();

        let mut rz = Vec::<u8>::new();
        rz.resize(record.get_padded_file_size() as usize, 0u8);

        data.read_exact(rz.as_mut_slice()).unwrap();

        return Some(rz);
    }

    pub fn map_texture_archives(&self, map_index: &str) -> Vec<NestedArchive> {
        let mut rz: Vec<NestedArchive> = vec![];

        for n in 0..4 {
            let archive = NestedArchive::new(
                self.data_by_name(&format!("/map/m{map_index}/m{map_index}_{n:0>4}.tpfbhd"))
                    .unwrap(),
                self.data_by_name(&format!("/map/m{map_index}/m{map_index}_{n:0>4}.tpfbdt"))
                    .unwrap(),
            );

            rz.push(archive);
        }

        rz
    }
}

/// Binder access, bhf3 + bdf3 files pair (ex.: .tpfbhd + .ptfbdt)
pub struct NestedArchive<'a> {
    index: Bhf3<'a>,
    data: Vec<u8>,
}

impl<'a> NestedArchive<'a> {
    pub fn new(index_data: Vec<u8>, data: Vec<u8>) -> NestedArchive<'a> {
        let index = Bhf3::new(index_data);

        NestedArchive { index, data }
    }

    /// Search archive for a record by name (file name w/o path and extension)
    pub fn lookup_data(&self, name_only: &str) -> Option<&[u8]> {
        for record in self.index.records {
            let name = self.index.record_name_by_offset(record.filename_offset);

            if name.contains(name_only) {
                let start = record.offset as usize;
                let end = start + record.compressed_size as usize;
                return Some(&self.data[start..end]);
            }
        }

        None
    }
}
