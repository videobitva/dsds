//! Formats definition, unpacker, converter

#![feature(effects)]
#![feature(const_for)]
#![feature(const_trait_impl)]
#![feature(const_mut_refs)]

use std::time::{SystemTime, UNIX_EPOCH};

pub mod format;

pub mod archive;
pub mod config;
pub mod fhash;
pub mod magic;

/// Current time in millis. Just a convenience method
pub fn millis() -> u128 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis()
}

/// Alias on `[f32; 2]` representing Vector2 in game content
pub type Vec2 = [f32; 2];

/// Alias on `[f32; 3]` representing Vector3 in game content
pub type Vec3 = [f32; 3];

/// Debug trait
pub trait Validate {
    fn validate(&self);
}
