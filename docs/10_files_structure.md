# Content structure

Relevant to DS1 only.

## Files on disk

Overview of known folders and files

`#` - some number or number range

| Path                         | File                        | Format             | Use/content                       |
|------------------------------|-----------------------------|--------------------|-----------------------------------|
| %steam%/%game%/DATA/         | DARKSOULS.exe               | Windows executable | -                                 |
|                              | dvdbnd0.bdt - dvdbnd3.bdt   | List of BDF3s      | "Binder" container data           |
|                              | dvdbnd0.bhd5 - dvdbnd3.bhd5 | BHD5               | "Binder" container header (index) |
|                              | fmod_event.dll              | Windows shared lib | -                                 |
|                              | fmodex.dll                  | Windows shared lib | -                                 |
|                              | steam_api.dll               | Windows shared lib | -                                 | 
| %appdata%/Local/NBGI/%game%/ | DarkSouls.ini               | INI config file    | Game config?                      |
| %docs%/NBGI/%game%/#/        | DRAKS000#.sl2               | ???                | Save file                         |

## Game archives

![](res/archive_format.png)

Each game archive is stored in two files:

- small (<100kbytes) dvdbnd#**.bhd5** archive index file;
- large (up to 1.8 gbytes) dvdbnd#**.bdt** archive data file;

**.bhd5** index file contains records (grouped in buckets (1) ) -- each record is just a tuple
of `(name hash, size, offset)`. Each record references one of data blocks in **.bdt** file.

**.bdt** is just a small `BDF3` header followed by a consequent data blocks witten one by one. One can guess type of
data looking at first 4 bytes of block.

(1): records sorted in buckets using this formula: `bucket_index = name_hash % bucket_count`. Bucket count is specified
in BHD5 header at the top of the file; I suppose the bucket count is chosen in such a way as to leave no more than 15
records in a group -- to speed up lookup.


### Game archives contents

It seems that game data was split between archives based on its type:

```
Archive 0. Levels + char
Archive 1. Parts + sounds
Archive 2. Events + scripts
Archive 3. Translations + game params
```

## FLVER

![](res/flver_structure.png)

Notes:
- Surface uses vertex buffer with the same index.
  But it is possible that there are more surfaces than vertex buffers. 
  Supposedly, in this case vertex buffer index is calculated as `vb_i = surf_i % vb_count`.


## Materials and shaders

![](res/material_to_shaders.png)

Notes:
- Is is unknown if SPX files are present in Release content or how
  to jump directly from .mtd to corresponding .vpo/.fpo pair;
