fn main() {
    // Workaround for `image_dds` compilation
    // See: https://stackoverflow.com/a/6045967
    println!("cargo:rustc-link-arg=-lstdc++");
}
