#![allow(dead_code)]

use crossbeam::channel::{Receiver, Sender};
use dantelion::{
    archive::RootArchive,
    config::{self, UserConfig},
    format::loadlist,
};
use godot::prelude::*;
use loader_thread::{MapLoadRequest, SendNode3D};

pub mod loader_thread;

struct MyExtension;

#[gdextension]
unsafe impl ExtensionLibrary for MyExtension {}

#[derive(GodotClass)]
#[class(base=Node)]
struct DsuInterface {
    base: Base<Node>,

    cfg: UserConfig,

    loader_in: Sender<MapLoadRequest>,
    loader_out: Receiver<(String, SendNode3D)>,
}

#[godot_api]
impl INode for DsuInterface {
    fn init(base: Base<Node>) -> Self {
        let cfg = config::get_user_config("./../user/cfg.json");

        let (loader_in, loader_out) = loader_thread::spawn_loader(cfg.clone());

        DsuInterface {
            base,
            cfg,
            loader_in,
            loader_out,
        }
    }

    fn physics_process(&mut self, _delta: f64) {
        let loader_out = self.loader_out.clone();

        loader_out.try_iter().for_each(|f| {
            let map_name = f.0.to_variant();
            let map_item = f.1 .0.to_variant();

            self.base_mut()
                .emit_signal("resource_loaded".into(), &[map_name, map_item]);
        });
    }
}

#[godot_api]
impl DsuInterface {
    #[func]
    fn get_maplist(&mut self) -> Array<Variant> {
        let mut items = VariantArray::new();

        let archive = RootArchive::new(&self.cfg, 0);

        let ll = loadlist::from(
            archive
                .data_by_name("/map/MapViewList.loadlistlist")
                .unwrap(),
        );

        for item in ll {
            items.push(
                dict! {
                    "path": item.correct_map_path(),
                    "name": item.map_basename(),
                    "debug_name": item.name.clone(),
                    "debug_path": item.path.clone(),
                }
                .to_variant(),
            );
        }

        items
    }

    #[func]
    fn request_load(&mut self, map_name: String) {
        godot_print!("GdExt: request load of {}", map_name);
        self.loader_in.send(MapLoadRequest { map_name }).unwrap();
    }

    #[signal]
    fn resource_loaded(map_name: String, resource: Gd<Node3D>);
}
