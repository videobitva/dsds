#![allow(dead_code)]

use std::{collections::HashMap, io::Cursor};

use crossbeam::channel::{Receiver, Sender};
use dantelion::{
    archive::{NestedArchive, RootArchive},
    config::UserConfig,
    format::{
        dcx,
        flver::{texture, Flver},
        msb1::{model_part_type::ModelPartType, Msb1},
        tpf::Tpf,
    },
    Vec3,
};

use godot::{
    builtin::{
        meta::ToGodot, PackedByteArray, PackedFloat32Array, PackedInt32Array, PackedVector2Array,
        PackedVector3Array, Variant, VariantArray, Vector2, Vector3,
    },
    engine::{
        base_material_3d::{CullMode, ShadingMode, TextureFilter, TextureParam, Transparency},
        mesh::{ArrayType, PrimitiveType},
        ArrayMesh, Image, ImageTexture, MeshInstance3D, Node3D, StandardMaterial3D,
    },
    obj::{Gd, IndexEnum, NewAlloc, NewGd},
};
use image_dds::{ddsfile::Dds, image::ImageOutputFormat};

const PI: f32 = std::f32::consts::PI;

pub struct MapLoadRequest {
    pub map_name: String,
}

#[derive(Debug, Default)]
struct LoaderCache {
    array_mesh: HashMap<String, Gd<ArrayMesh>>,
    texture: HashMap<String, Gd<ImageTexture>>,
}

fn level_loading_thread(
    cfg: UserConfig,
    requests: Receiver<MapLoadRequest>,
    output: Sender<(String, SendNode3D)>,
) {
    println!("Heavy level loader: started");

    let mut cache = LoaderCache::default();

    loop {
        let rq = requests.recv();
        if rq.is_err() {
            break;
        }

        let rq = rq.unwrap();
        let map_name = rq.map_name;

        println!("Heavy level loader: loading {map_name}");

        let archive = RootArchive::new(&cfg, 0);

        let record = archive.record_by_name(&format!("/map/MapStudio/{map_name}.msb"));
        if record.is_none() {
            println!("Map doesnt exist");
            continue;
        }

        let record = record.unwrap();

        let map_index = &map_name[1..3];
        let tex_archives = archive.map_texture_archives(map_index);

        let msb = Msb1::new(archive.data_by_hash(record.file_name_hash).unwrap());

        let parts = msb.parts();
        let models = msb.models();

        for idx in 0..parts.entry_count() {
            let part = parts.entry_by_idx(idx);
            if part.draw_groups == [0u32; 4] {
                continue;
            }

            let model = models.entry_by_idx(part.model_index as usize);

            let flver_path;

            if part.entry_type == ModelPartType::MapPiece {
                flver_path = model.map_flver_path(&map_name);
            } else {
                continue;
            }

            let node = read_flver(&flver_path, &archive, &tex_archives, &mut cache);

            if node.is_none() {
                continue;
            }
            let mut node = node.unwrap();

            node.set_position(v2v(part.position));
            node.set_rotation(v2v(part.rotation));
            node.set_scale(v2v(part.scale));

            let l = output.send((map_name.clone(), SendNode3D(node)));
            if l.is_err() {
                return;
            }
        }

        println!("Heavy level loader: served");
    }
}

fn v2v(vec: Vec3) -> Vector3 {
    Vector3::new(vec[0], vec[1], vec[2])
}

fn read_flver(
    flver_path: &String,
    archive: &RootArchive,
    texture_archives: &Vec<NestedArchive>,
    cache: &mut LoaderCache,
) -> Option<Gd<Node3D>> {
    let compressed = archive.data_by_name(&flver_path);
    if compressed.is_none() {
        println!(" Doesnt exist, skipping");
        return None;
    }

    let dcx = compressed.unwrap();
    let flver = Flver::new(dcx::decompress(&dcx));

    let mut root_node = Node3D::new_alloc();

    for mesh_index in 0..flver.header.mesh_count {
        let cache_key = format!("{}#{}", flver_path, mesh_index);

        let mut mesh_instance = MeshInstance3D::new_alloc();

        let mut node_mesh;

        if cache.array_mesh.contains_key(&cache_key) {
            node_mesh = cache.array_mesh.get(&cache_key).unwrap().clone();
        } else {
            node_mesh = ArrayMesh::new_gd();

            let mesh = flver.mesh(mesh_index);

            let mat = mesh.header.material_index;
            let diffuse = flver.texture_by_type(mat, texture::DIFFUSE);
            let bump = flver.texture_by_type(mat, texture::BUMPMAP);
            let specular = flver.texture_by_type(mat, texture::SPECULAR);

            for i in 0..mesh.surface_indices.len() {
                let mut surface_array = VariantArray::new();
                surface_array.resize(ArrayType::MAX.to_index(), &Variant::nil());

                let surface = flver.surface(mesh.surface_indices[i]);
                let vert_buf_index =
                    mesh.vertex_buffer_indices[i % mesh.vertex_buffer_indices.len()];
                let vert_buf = flver.vertex_buffer(vert_buf_index);

                let layout = flver.buffer_layout(vert_buf.header.layout_index);

                // Copy positions
                {
                    let mut array_vertex = PackedVector3Array::new();

                    let lm_position = layout.position();
                    for vertex in vert_buf.vertices() {
                        let position = lm_position.extract_position(vertex);
                        array_vertex.push(Vector3::new(position[0], position[1], position[2]));
                    }
                    surface_array.set(ArrayType::VERTEX.to_index(), array_vertex.to_variant());
                }

                // Copy normals
                if let Some(lm_normal) = layout.normal() {
                    let mut array_normal = PackedVector3Array::new();

                    for vertext_data in vert_buf.vertices() {
                        let normal = lm_normal.extract_4b(vertext_data);
                        array_normal.push(Vector3::new(
                            normal[0].into(),
                            normal[1].into(),
                            normal[2].into(),
                        ));
                    }

                    surface_array.set(ArrayType::NORMAL.to_index(), array_normal.to_variant());
                }

                // Copy tangents
                if let Some(lm_tangent) = layout.tangent() {
                    let mut array_tangent = PackedFloat32Array::new();

                    for vertext_data in vert_buf.vertices() {
                        let tangent = lm_tangent.extract_4b(vertext_data);
                        array_tangent.push((tangent[0] as i8).into());
                        array_tangent.push((tangent[1] as i8).into());
                        array_tangent.push((tangent[2] as i8).into());
                        array_tangent.push((tangent[3] as i8).into());
                    }

                    surface_array.set(ArrayType::TANGENT.to_index(), array_tangent.to_variant());
                }

                // Copy UVs
                let mut uv = None;
                let uv_pair = layout.uv_pair();
                if uv_pair.is_some() {
                    uv = uv_pair;
                } else {
                    let uv_single = layout.uv();
                    if uv_single.is_some() {
                        uv = uv_single;
                    }
                }

                if let Some(lm_uv) = uv {
                    let mut array_uv = PackedVector2Array::new();

                    for vertex in vert_buf.vertices() {
                        let uv = lm_uv.extract_2s(vertex);
                        let divisor = 1024.0;

                        let uv = [
                            (f32::from(uv[0]) / divisor), // * tex.header.scale[0],
                            (f32::from(uv[1]) / divisor), // * tex.header.scale[1],
                        ];

                        array_uv.push(Vector2::new(uv[0], uv[1]));
                    }

                    surface_array.set(ArrayType::TEX_UV.to_index(), array_uv.to_variant());
                }

                // Copy vertex indices
                {
                    let mut array_index = PackedInt32Array::new();

                    // Skip all the weird surfaces (for now)
                    if surface.header.flags != 0 {
                        continue;
                    }

                    for index in surface.indices {
                        array_index.push(i32::from(*index));
                    }
                    surface_array.set(ArrayType::INDEX.to_index(), array_index.to_variant());
                }

                let primitive = if surface.header.triangle_strip {
                    PrimitiveType::TRIANGLE_STRIP
                } else {
                    PrimitiveType::TRIANGLES
                };

                node_mesh.add_surface_from_arrays(primitive, surface_array);

                let mut mat = StandardMaterial3D::new_gd();
                mat.set_transparency(Transparency::ALPHA_SCISSOR);
                mat.set_texture_filter(TextureFilter::NEAREST_WITH_MIPMAPS_ANISOTROPIC);

                mat.set_cull_mode(if surface.header.cull_backfaces {
                    CullMode::BACK
                } else {
                    CullMode::DISABLED
                });

                if diffuse.is_some() {
                    let tex = read_texture(&diffuse.clone().unwrap(), texture_archives, cache);

                    if tex.is_some() {
                        let tex = tex.unwrap();
                        mat.set_texture(TextureParam::ALBEDO, tex.upcast());
                    }
                }

                if false && bump.is_some() {
                    let tex = read_texture(&bump.clone().unwrap(), texture_archives, cache);

                    if tex.is_some() {
                        let tex = tex.unwrap();
                        mat.set_texture(TextureParam::NORMAL, tex.upcast());
                    }
                }

                if false && specular.is_some() {
                    let tex = read_texture(&specular.clone().unwrap(), texture_archives, cache);

                    if tex.is_some() {
                        let tex = tex.unwrap();
                        mat.set_texture(TextureParam::METALLIC, tex.upcast());
                    }
                }

                let surf_id = node_mesh.get_surface_count() - 1;
                node_mesh.surface_set_material(surf_id, mat.upcast());
            }

            cache.array_mesh.insert(cache_key, node_mesh.clone());
        }

        mesh_instance.set_mesh(node_mesh.clone().upcast());

        root_node.add_child(mesh_instance.clone().upcast());
    }

    Some(root_node)
}

fn read_texture(
    tex: &dantelion::format::flver::texture::Texture,
    texture_archives: &Vec<NestedArchive>,
    cache: &mut LoaderCache,
) -> Option<Gd<ImageTexture>> {
    let tex_path = tex.normal_path();

    if cache.texture.contains_key(&tex_path) {
        return Some(cache.texture.get(&tex_path).unwrap().clone());
    } else {
        let file_path = tex_path.clone();
        let last_shash = file_path.rfind("/").unwrap() + 1;
        let dot = file_path.find(".").unwrap() + 1;

        let looking_for = &file_path[last_shash..dot];

        for archive in texture_archives {
            let data = archive.lookup_data(looking_for);

            if data.is_some() {
                let compressed = data.unwrap();
                let data = dcx::decompress(compressed);

                let tpf = Tpf::new(data);

                let dds = Dds::read(tpf.dds(0)).unwrap();
                let mipmap_count = dds.get_num_mipmap_levels();
                let image = image_dds::image_from_dds(&dds, mipmap_count-1).unwrap();

                let mut dst = Cursor::new(Vec::<u8>::new());
                image.write_to(&mut dst, ImageOutputFormat::Png).unwrap();

                let bytearray = PackedByteArray::from(dst.get_ref().as_slice());
                let mut img = Image::new_gd();
                img.load_png_from_buffer(bytearray);

                let tex = ImageTexture::create_from_image(img).unwrap();
                cache.texture.insert(tex_path.clone(), tex.clone());

                return Some(tex);
            }
        }
    }

    println!("Failed to get {tex_path}");
    None
}

pub fn spawn_loader(cfg: UserConfig) -> (Sender<MapLoadRequest>, Receiver<(String, SendNode3D)>) {
    let (rq_tx, rq_rx) = crossbeam::channel::unbounded();
    let (rz_tx, rz_rx) = crossbeam::channel::unbounded();

    std::thread::spawn(move || level_loading_thread(cfg, rq_rx, rz_tx));

    (rq_tx, rz_rx)
}

pub struct SendNode3D(pub Gd<Node3D>);

unsafe impl Send for SendNode3D {}
