#![allow(dead_code)]

use dantelion::{
    archive::RootArchive,
    config::{self, UserConfig},
    fhash,
    format::{
        bnd3::Bnd3,
        dcx,
        flver::{self, Flver},
        msb1::{model_part_type::ModelPartType, part::ObjectTypeData, Msb1},
        tpf::Tpf,
    },
    magic, millis,
};

fn main() {
    let cfg = config::get_user_config("./user/cfg.json");
    if cfg.archive_names.is_some() {
        fhash::fill_revhash_map(&cfg.archive_names.clone().unwrap());
    }

    let s = millis();
    models(&cfg);

    let e = millis();
    println!("Elapsed {} ms", (e - s));
}

fn models(cfg: &UserConfig) {
    let basename = "m14_00_00_00";

    let archive_0 = RootArchive::new(cfg, 0);
    let archive_1 = RootArchive::new(cfg, 1);

    let map_path = format!("/map/MapStudio/{basename}.msb");
    let msb = Msb1::new(archive_0.data_by_name(&map_path).unwrap());

    let parts = msb.parts();

    for part_idx in 0..parts.entry_count() {
        let part = parts.entry_by_idx(part_idx);

        if part.entry_type == ModelPartType::Object {
            let name = part.name();
            let path = format!("/obj/{}.objbnd.dcx", &name[0..5]);
            println!("Looking for {}", path);

            let objbnd = dcx::decompress(&archive_1.data_by_name(&path).unwrap());
            let binder = Bnd3::new(objbnd);
            println!("Binder flags {:b}", binder.flags());

            println!("\nRecords:");
            for record in binder.record_iter() {
                println!("  {} at 0x{:x}", record.name(), record.data_offset);
            }

            println!("\nTpf:");
            let tpf_data = binder.lookup(".tpf").unwrap().to_vec();
            let tpf = Tpf::new(tpf_data);
            for texture in tpf.texture_iter() {
                println!("  {}", texture.header.file_size);
            }

            println!("\nFlver:");
            let flver_data = binder.lookup(".flver").unwrap().to_vec();
            let flver = Flver::new(flver_data);
            flver.print_contents();

            break;
        }
    }
}
