extends Node3D

var mouse_sense = 0.0025
var move_speed = 80.0

## Mirror camera on X axis.
## True by default as DS' coordinate system is left-handed.
@export var mirror: bool = true

@onready var root: Root = $"../"
@onready var dsu: DsuInterface = $"../DsuInterface"
@onready var cam: Camera3D = $Camera3D

var maplist = [];
var mapnames = [
	"Depths",
	"Undead Burg",
	"Firelink Shrine",
	"Painted World",
	"Darkroot Garden",
	"Sanctuary Garden",
	"Catacombs",
	"Tomb of the Giants",
	"Great Hollow",
	"Blighttown",
	"Demon Ruins",
	"Sen's Fortress",
	"Anor Londo",
	"New Londo Ruins",
	"Duke's Archives",
	"The Kiln of the First Flame",
	"Northern Undead Asylum"
];

func _ready():
	$DebugMaplist.visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	_set_mirror(mirror)
	
	maplist = dsu.get_maplist() as Array;
	var mapitemlist = $DebugMaplist/Panel/ItemList as ItemList
	for i in range(len(maplist)):
		mapitemlist.add_item(format_mapname(i, false))

func format_mapname(i: int, active: bool):
	var checkbox = "[x] " if active else "[ ] "
	
	var mapname = maplist[i]["debug_name"]
	if len(mapnames) > i and mapnames[i] is String:
		mapname = mapnames[i]
	
	return checkbox + maplist[i]["name"] + " | " + mapname


func _set_mirror(value: bool):
	mirror = value
	$Camera3D.scale.z = -1 if value else 1


func _input(event):
	if event is InputEventMouseMotion and not $DebugMaplist.visible:
		var mirror_mod = -1 if mirror else 1
		rotation.y -= event.relative.x * mouse_sense * mirror_mod
		cam.rotation.x -= event.relative.y * mouse_sense * mirror_mod
		cam.rotation.x = clampf(cam.rotation.x, -PI/2, PI/2)
	
	if event.is_action_released("debug_toggle_maplist"):
		toggle_map_list()
	
	if event.is_action_pressed("speed_up"):
		move_speed *= 1.2
	
	if event.is_action_pressed("speed_down"):
		move_speed /= 1.2


func toggle_map_list():
	$DebugMaplist.visible = not $DebugMaplist.visible
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE if $DebugMaplist.visible else Input.MOUSE_MODE_CAPTURED)


func _process(delta):
	var move_x = Input.get_axis("move_left", "move_right")
	var move_z = Input.get_axis("move_forward", "move_backward")
	var move_y = Input.get_axis("move_down", "move_up")
	var move = Vector3(move_x, move_y, move_z).normalized()
	global_position += (cam.to_global(move) - cam.to_global(Vector3.ZERO)) * move_speed * delta


func _on_map_item_toggled(index):
	var lvl_active = root.toggle_map_loading(maplist[index].name)
	toggle_map_list()
	
	var mapitemlist = $DebugMaplist/Panel/ItemList as ItemList
	mapitemlist.set_item_text(index, format_mapname(index, lvl_active))
