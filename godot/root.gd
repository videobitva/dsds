class_name Root
extends Node3D

@onready var dsu: DsuInterface = $DsuInterface

var pref_cube = preload("res://cube.tscn")

func _ready():
	var maplist = dsu.get_maplist() as Array;
	#for map in maplist:j
		#dsu.request_load(map.name)


func _on_dsu_interface_resource_loaded(map_name: String, resource: Node3D):
	var map = get_node_or_null(map_name)
	if map != null:
		map.call_deferred("add_child", resource);
	


## Toggles map. Returns true if map will be loaded now
func toggle_map_loading(map_name: String) -> bool:
	var map = get_node_or_null(map_name)
	if map == null:
		map = Node3D.new()
		map.name = map_name
		add_child(map)
		dsu.request_load(map_name)
		return true
	else:
		map.queue_free()
		return false
