#!/usr/bin/python3

magic = 0.45

points = [
  (0, 0, 0),

  (0.25, 0, -magic),
  (0.25,-magic,0),
  (0.25, 0, magic),
  (0.25, magic, 0),

  (0.5, 0,0),
  (0.5, -magic, -magic),
  (0.5, -magic, magic),
  (0.5, magic, -magic),
  (0.5, magic, magic),

  (0.75, -magic, 0),
  (0.75, 0, -magic),
  (0.75, magic, 0),
  (0.75, 0, magic),

  (1,0,0)
]

def get_closest(x, y, z):
  # closest_x = ((x * 40) // 10) * 0.25

  found_dist = 100
  found_point = (0,0,0)

  for (bx, by, bz) in points:
    # if closest_x != bx: continue

    dist = ( (bx-x)**2 + (by-y)**2 + (bz-z)**2 ) ** 0.5
    if dist < found_dist:
      found_dist = dist
      found_point = (bx, by, bz)
  
  return found_point



def align(y, cr, cb):
  y = float(y) / 255.0

  cr = float(cr) / 255.0
  cr -= 0.5

  cb = float(cb) / 255.0
  cb -= 0.5

  (y, cr, cb) = get_closest(y, cr, cb)

  return (
    int(y * 255),
    int( (cr + 0.5) * 255 ),
    int( (cb + 0.5) * 255 )
  )



from PIL import Image

h = 33
w = h*h

bitmap = []

for y in range(h):
  g = (float(y) / float(h))

  for x in range(w):
    r = float(x % h) / float(h)
    b = 0

    l = float(int(x) // int(h))
    if l > 0:
      b = float(int(x) // int(h)) / h

    rgb = (
      int(r * 255), 
      int(g * 255), 
      int(b * 255)
    )

    bitmap.append(rgb)


image = Image.new('RGB', (w,h))
image.putdata(bitmap)
image = image.convert('YCbCr')

for px_y in range(h):
  for px_x in range(w):
    (y, cr, cb) = image.getpixel((px_x, px_y))
    image.putpixel((px_x, px_y), align(y, cr, cb))

image = image.convert('RGB')
image.save('lut.png')

