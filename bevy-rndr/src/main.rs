#![feature(path_file_prefix)]

mod loader;

use std::f32::consts::PI;

use bevy::{
    diagnostic::{
        Diagnostic, DiagnosticPath, Diagnostics, RegisterDiagnostic,
    },
    prelude::*,
    render::view::RenderLayers,
    window::PresentMode,
};
use bevy_flycam::prelude::*;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_screen_diagnostics::{
    Aggregate, ScreenDiagnostics, ScreenDiagnosticsPlugin, ScreenEntityDiagnosticsPlugin,
    ScreenFrameDiagnosticsPlugin,
};
use loader::DantalionPlugin;

fn setup_lightning(mut commands: Commands) {
    // directional 'sun' light
    commands.spawn(DirectionalLightBundle {
        directional_light: DirectionalLight {
            illuminance: light_consts::lux::FULL_DAYLIGHT,
            shadows_enabled: true,
            ..default()
        },
        transform: Transform {
            translation: Vec3::new(0.0, 2.0, 0.0),
            rotation: Quat::from_rotation_x(-PI / 2.),
            ..default()
        },
        // cascade_shadow_config: CascadeShadowConfigBuilder {
        //     first_cascade_far_bound: 4.0,
        //     maximum_distance: 2.0,
        //     ..default()
        // }
        // .into(),
        ..default()
    });

    commands.insert_resource(AmbientLight {
        color: Color::WHITE,
        brightness: 30.0,
    });
}

const MESH_COUNT: DiagnosticPath = DiagnosticPath::const_new("mesh_count");
const VISIBLE_MESH_COUNT: DiagnosticPath = DiagnosticPath::const_new("visible_mesh_count");

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: "DSDS Bevy Renderer".into(),
                present_mode: PresentMode::AutoNoVsync,
                ..default()
            }),
            ..default()
        }))
        .add_plugins(WorldInspectorPlugin::new())
        .add_plugins(PlayerPlugin)
        .add_plugins(DantalionPlugin)
        .register_diagnostic(Diagnostic::new(MESH_COUNT))
        .register_diagnostic(Diagnostic::new(VISIBLE_MESH_COUNT))
        .add_plugins((
            ScreenDiagnosticsPlugin {
                render_layer: RenderLayers::all(),
                style: Style {
                    align_self: AlignSelf::Center,
                    position_type: PositionType::Absolute,
                    bottom: Val::Px(5.0),
                    ..default()
                },
                ..default()
            },
            ScreenFrameDiagnosticsPlugin,
            ScreenEntityDiagnosticsPlugin,
        ))
        .add_systems(Startup, (setup_lightning, setup_diagnostic))
        .add_systems(Update, (mesh_count, visible_mesh_count))
        .run();
}

fn setup_diagnostic(mut onscreen: ResMut<ScreenDiagnostics>) {
    onscreen
        .add("Meshes".to_string(), MESH_COUNT)
        .aggregate(Aggregate::Value)
        .format(|v| format!("{v}"));

    onscreen
        .add("Visible Meshes".to_string(), VISIBLE_MESH_COUNT)
        .aggregate(Aggregate::Value)
        .format(|v| format!("{v:.0}"));
}

// System for printing the number of meshes on every tick of the timer
fn visible_mesh_count(
    mut diagnostics: Diagnostics,
    sprites: Query<(&Handle<Mesh>, &ViewVisibility)>,
) {
    diagnostics.add_measurement(&VISIBLE_MESH_COUNT, || {
        sprites.iter().filter(|(_, vis)| vis.get()).count() as f64
    });
}

fn mesh_count(mut diagnostics: Diagnostics, sprites: Query<(&Handle<Mesh>, &ViewVisibility)>) {
    diagnostics.add_measurement(&MESH_COUNT, || sprites.iter().len() as f64);
}
