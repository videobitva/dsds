use std::path::Path;

use dantelion::{
    archive::RootArchive,
    config::UserConfig,
    format::{
        bhf3::Bhf3,
        dcx,
        flver::{
            texture::{self, Texture},
            Flver,
        },
        loadlist,
        msb1::{model_part_type::ModelPartType, Msb1},
        tpf::Tpf,
    },
    magic,
};

use bevy::{
    prelude::*,
    render::{
        mesh::{Indices, PrimitiveTopology},
        render_asset::RenderAssetUsages,
        render_resource::Face,
        texture::{CompressedImageFormats, ImageAddressMode, ImageFormat, ImageSampler, ImageSamplerDescriptor, ImageType},
    },
    utils::HashMap,
};

use serde::{Deserialize, Serialize};

#[derive(Component, Deref, Serialize, Deserialize, Reflect)]
#[reflect(Component, Serialize, Deserialize)]
pub struct LevelName(pub String);

#[derive(Component, Deref, Serialize, Deserialize, Reflect)]
#[reflect(Component, Serialize, Deserialize)]
pub struct LevelPath(pub String);

#[derive(Component, Default, Serialize, Deserialize, Reflect)]
#[reflect(Component, Serialize, Deserialize)]
pub enum LevelState {
    ToLoad,
    Loaded,
    ToUnload,
    #[default]
    Unloaded,
}

#[derive(Component)]
pub struct ToLoad;

#[derive(Component)]
pub struct Loaded;

#[derive(Component)]
pub struct ToUnload;

#[allow(dead_code)]
#[derive(Component)]
pub struct Unloaded;

#[derive(Resource, Deref)]
pub struct DantalionArchive(pub RootArchive<'static>);

#[derive(Resource, Deref, DerefMut, Default)]
pub struct TextureCache(pub HashMap<String, Handle<Image>>);

impl Default for DantalionArchive {
    fn default() -> Self {
        let cfg = UserConfig {
            game_root: "/run/media/videobitva/32F6A5BAF6A57EAB/Program Files (x86)/BANDAI NAMCO Games/DARK SOULS - Prepare To Die Edition/DATA".into(),
           ..Default::default()
        };

        Self(RootArchive::new(&cfg, 0))
    }
}

pub struct DantalionPlugin;

impl Plugin for DantalionPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(DantalionArchive::default());
        app.register_type::<LevelName>();
        app.register_type::<LevelPath>();
        app.register_type::<LevelState>();
        app.add_systems(Startup, load_level);
        app.add_systems(PostStartup, init_levels);
        app.add_systems(Update, (load_levels, unload_levels));
    }
}

pub fn init_levels(mut commands: Commands, archive: Res<DantalionArchive>) {
    let levels = loadlist::from(
        archive
            .data_by_name("/map/MapViewList.loadlistlist")
            .unwrap(),
    );

    let levels = levels
        .into_iter()
        .map(|level| {
            let name = LevelName(level.name.clone());
            let path = LevelPath(level.correct_map_path());
            let state = LevelState::default();

            (name, path, state)
        })
        .collect::<Vec<_>>();

    commands.spawn_batch(levels);
}

pub fn load_levels(
    mut commands: Commands,
    levels: Query<(Entity, &LevelPath, &LevelName), With<ToLoad>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    for (entity, _, _) in levels.iter_inner() {
        let mut material = StandardMaterial::from(Color::GRAY);
        material.cull_mode = Some(Face::Front);
        let mat_handle = materials.add(material);

        let bevy_mesh = Mesh::new(
            PrimitiveTopology::TriangleStrip,
            RenderAssetUsages::RENDER_WORLD,
        );

        commands
            .entity(entity)
            .insert(PbrBundle {
                mesh: meshes.add(bevy_mesh),
                material: mat_handle.clone(),
                ..Default::default()
            })
            .remove::<ToLoad>()
            .insert(Loaded);
    }
}


pub fn unload_levels(
    mut _commands: Commands,
    _levels: Query<(Entity, &LevelPath, &LevelName), With<ToUnload>>,
) {
    // levels.into_iter().map(|(entity, path, name)| {
    //     commands.entity(entity).remove::<PbrBundle>();
    // });
}

pub fn load_level(
    mut commands: Commands,
    // mut level: Query<&LevelId>,
    // levels: Res<LevelArchive>,
    archive: Res<DantalionArchive>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut textures: ResMut<Assets<Image>>,
) {
    let mut std_material = StandardMaterial::from(Color::GRAY);
    std_material.cull_mode = Some(Face::Front);
    let std_material = materials.add(std_material);

    let _ll = loadlist::from(
        archive
            .data_by_name("/map/MapViewList.loadlistlist")
            .unwrap(),
    );

    // for entry in ll {
    //     let record = archive.record_by_name(&entry.correct_map_path());
    //     if record.is_none() {
    //         continue;
    //     }
    let record = archive.record_by_name("/map/MapStudio/m14_00_00_00.msb");

    let record = record.unwrap();
    // let map_path = entry.correct_map_path();
    // let map_name = Msb1::map_basename(&map_path);
    let map_name = "m14_00_00_00";
    log::info!("Map name is {}", map_name);

    let msb = Msb1::new(archive.data_by_hash(record.file_name_hash).unwrap());

    let parts = msb.parts();
    let models = msb.models();

    let map_index = &map_name[1..3];
    let archive_headers: Vec<Bhf3> = (0..4)
        .into_iter()
        .map(|n| {
            Bhf3::new(
                archive
                    .data_by_name(&format!("/map/m{map_index}/m{map_index}_{n:0>4}.tpfbhd"))
                    .unwrap(),
            )
        })
        .collect();

    let archive_datas: Vec<Vec<u8>> = (0..4)
        .into_iter()
        .map(|n| {
            archive
                .data_by_name(&format!("/map/m{map_index}/m{map_index}_{n:0>4}.tpfbdt"))
                .unwrap()
        })
        .collect();

    for idx in 0..parts.entry_count() {
        let part = parts.entry_by_idx(idx);
        let model = models.entry_by_idx(part.model_index as usize);

        let flver_path;

        if part.entry_type == ModelPartType::MapPiece {
            flver_path = model.map_flver_path(map_name);
            log::info!("Loading {}", flver_path);
        } else {
            continue;
        }

        let compressed = archive.data_by_name(&flver_path);
        if compressed.is_none() {
            log::info!(" Doesnt exist, skipping");
            continue;
        }

        let dcx = compressed.unwrap();
        let flver = Flver::new(dcx::decompress(&dcx));

        let batch = get_meshes(&flver)
            .into_iter()
            .map(|(mesh, mat)| {
                let mat = load_material(
                    mat,
                    &flver,
                    &archive_headers,
                    &archive_datas,
                    &mut materials,
                    &mut textures,
                )
                .unwrap_or(std_material.clone());

                (
                    PbrBundle {
                        mesh: meshes.add(mesh),
                        material: mat,
                        ..Default::default()
                    },
                    // ShowAabbGizmo::default(),
                )
            })
            .collect::<Vec<_>>();

        commands.spawn_batch(batch);
    }
    // }

    log::info!("Heavy level loader: served");
}

pub fn get_meshes(flver: &Flver) -> Vec<(Mesh, i32)> {
    let mut res = Vec::new();

    for mesh_index in 0..flver.header.mesh_count {
        let mesh = flver.mesh(mesh_index);

        let mat = mesh.header.material_index;

        let mut bevy_mesh = Mesh::new(
            PrimitiveTopology::TriangleStrip,
            RenderAssetUsages::RENDER_WORLD,
        );

        for i in 0..mesh.surface_indices.len() {
            if i >= mesh.vertex_buffer_indices.len() {
                continue;
            }

            let vert_buf_index = mesh.vertex_buffer_indices[i];

            let vert_buf = flver.vertex_buffer(vert_buf_index);
            let layout = flver.buffer_layout(vert_buf.header.layout_index);

            // Insert vertices into bevy mesh
            {
                let lm_position = layout.position();
                let array_vertex = vert_buf
                    .vertices()
                    .map(|vertext_data| {
                        Vec3::from_slice(lm_position.extract_position(vertext_data))
                    })
                    .collect::<Vec<_>>();

                bevy_mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, array_vertex);
            }

            // Insert normals into bevy mesh
            if let Some(lm_normal) = layout.normal() {
                let array_normal = vert_buf
                    .vertices()
                    .map(|vertext_data| {
                        let normal = lm_normal.extract_4b(vertext_data);
                        Vec3::new(normal[0].into(), normal[1].into(), normal[2].into())
                    })
                    .collect::<Vec<_>>();

                bevy_mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, array_normal);
            }

            // Insert tangents into bevy mesh
            // maybe replace it with `bevy_mesh.generate_tangents();`
            if let Some(lm_tangent) = layout.tangent() {
                let array_normal = vert_buf
                    .vertices()
                    .map(|vertext_data| {
                        let normal = lm_tangent.extract_4b(vertext_data);
                        Vec4::new(
                            normal[0].into(),
                            normal[1].into(),
                            normal[2].into(),
                            normal[3].into(),
                        )
                    })
                    .collect::<Vec<_>>();

                bevy_mesh.insert_attribute(Mesh::ATTRIBUTE_TANGENT, array_normal);
            }

            // Insert UVs
            if let Some(lm_uv) = layout.uv_pair().or(layout.uv()) {
                const DIVIDER: f32 = 1024.0;
                let array_uv = vert_buf
                    .vertices()
                    .map(|vertext_data| {
                        let uv = lm_uv.extract_2s(vertext_data);
                        Vec2::new(uv[0].into(), uv[1].into()) / DIVIDER
                    })
                    // .inspect(|uv| log::info!("UV: {uv:?}"))
                    .collect::<Vec<_>>();

                bevy_mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, array_uv);
            }

            // Insert indices into bevy mesh
            let surface_set = flver.surface(mesh.surface_indices[i]);

            // Skip all the weird surfaces (for now)
            if surface_set.header.flags != 0 {
                continue;
            }

            let indices = surface_set
                .indices
                .iter()
                .map(|index| *index)
                .collect::<Vec<_>>();

            bevy_mesh.insert_indices(Indices::U16(indices));
        }

        res.push((bevy_mesh, mat));
    }

    res
}

fn load_material(
    material_id: i32,
    flver: &Flver,
    archive_headers: &Vec<Bhf3>,
    archive_datas: &Vec<Vec<u8>>,
    materials: &mut ResMut<Assets<StandardMaterial>>,
    textures: &mut ResMut<Assets<Image>>,
) -> Option<Handle<StandardMaterial>> {
    let load_texture = |texture: Texture| {
        let tex_path = texture.normal_path();
        let path = Path::new(tex_path.as_str()).file_prefix()?.to_str()?;

        let (index, _, record) = archive_headers
            .iter()
            .enumerate()
            .map(|(index, header)| {
                header
                    .records
                    .iter()
                    .map(move |record| (index, header, record))
            })
            .flatten()
            .find(|(_, header, record)| {
                let name =
                    unsafe { magic::ascii_by_vec_offset(&header.data, record.filename_offset) };

                name.contains(path)
            })?;

        let start = record.offset as usize;
        let end = start + record.compressed_size as usize;
        let compressed = (&archive_datas[index][start..end]).to_vec();
        let data = dcx::decompress(&compressed);
        let dds_raw = Tpf::new(data).dds(0).to_vec();

        // Image::n

        Image::from_buffer(
            #[cfg(all(debug_assertions))]
            path.to_string(),
            dds_raw.as_ref(),
            ImageType::Format(ImageFormat::Dds),
            CompressedImageFormats::all(),
            false,
            ImageSampler::Descriptor(ImageSamplerDescriptor {
                address_mode_u: ImageAddressMode::Repeat,
                address_mode_v: ImageAddressMode::Repeat,
                ..Default::default()
            }),
            RenderAssetUsages::RENDER_WORLD,
        )
        .ok()
    };

    let diffuse = flver
        .texture_by_type(material_id, texture::DIFFUSE)
        .and_then(load_texture)
        .map(|txt| textures.add(txt));
    let bump = flver
        .texture_by_type(material_id, texture::BUMPMAP)
        .and_then(load_texture)
        .map(|txt| textures.add(txt));
    let specular = flver
        .texture_by_type(material_id, texture::SPECULAR)
        .and_then(load_texture)
        .map(|txt| textures.add(txt));


    Some(materials.add(StandardMaterial {
        base_color_texture: diffuse,
        metallic_roughness_texture: specular,
        normal_map_texture: bump,
        cull_mode: Some(Face::Front),
        ..Default::default()
    }))
}
