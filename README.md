# DSDS

Blazingly fast but almost useless DS1 map viewer.

[Documentation](https://somobu.gitlab.io/dsds/book/index.html) | [Reference](https://somobu.gitlab.io/dsds/doc/dantelion/index.html) | [Dev videos](https://www.youtube.com/playlist?list=PLPT7KABCtFvxiAJ53m-8yKnMyfb9IEac9)


## How to build and run

First of all, you'll need a Steam version of DS1 (PTDE) installed.

`cargo run` once, adjust generated `user/cfg.json` config.

To use `godot`, you have to import project into Godot v4.2+ and build lib in `--release` mode.


## Backlog

Objectives:
- map collision;
- map props (MSB 'object' type - objbnd);

Unresolved:
- how to properly access texture data (tga) from flver texture header?
- how to jump from material definition (.mtd) to corresponding shader pair (.vpo + .fpo)?
- Undead Burg and Duke's Archives placed incorrectly -- is there some obscude MSB data somewhere else?
- the use of msb1/part draw & disp groups are unknown;

Unimplmeneted:
- msb1: events and regions are not implemented and tested;

Future:
- replace all `assert` w/ `debug_assert`;
- use original shaders (requires custom render. [DXVK-native](https://github.com/Joshua-Ashton/dxvk-native)?);

Long term:
- Ash Lake -- `map/m13_01_00_00/`
- Quelaag's Domain (Fair Lady) -- `map/m14_00_00_00/m51x0B0A14.flver` (ex.: `m5100B0A14.flver`)
- Majula
- Devotee Scarlett


## Credits

- [Souls Formats](https://github.com/JKAnderson/SoulsFormats/tree/master) by [Joseph Anderson](https://github.com/JKAnderson);
- [dstools](https://github.com/katalash/dstools/) by [katalash](https://github.com/katalash);
- [ds_extract_and_pack](https://github.com/MasonM/ds_extract_and_pack) by [Mason Malone](https://github.com/MasonM);
- [blender-flver](https://github.com/kotn3l/blender-flver) by [Eliza](https://github.com/elizagamedev) and [kotn3l](https://github.com/kotn3l)


## License

[GNU GPL v3](https://gitlab.com/somobu/dsds/-/blob/master/LICENSE?ref_type=heads)
